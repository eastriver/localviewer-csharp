﻿namespace LocalViewer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.appStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.addressStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lnglatStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.nameTBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addressTBox = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.longitudeTBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.latitudeTBox = new System.Windows.Forms.TextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.linkManTBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.linkPhoneTBox = new System.Windows.Forms.TextBox();
            this.editPanel = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.idTBox = new System.Windows.Forms.TextBox();
            this.aMapBrowser = new System.Windows.Forms.WebBrowser();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshMItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutMItem = new System.Windows.Forms.ToolStripMenuItem();
            this.explorerPanel = new System.Windows.Forms.Panel();
            this.explorerLBox = new System.Windows.Forms.CheckedListBox();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.addToolBtn = new System.Windows.Forms.ToolStripButton();
            this.editToolBtn = new System.Windows.Forms.ToolStripButton();
            this.deleteToolBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mapToolBtn = new System.Windows.Forms.ToolStripButton();
            this.tableToolBtn = new System.Windows.Forms.ToolStripButton();
            this.tablePanel = new System.Windows.Forms.Panel();
            this.explorerTable = new System.Windows.Forms.DataGridView();
            this.pointSource = new System.Windows.Forms.BindingSource(this.components);
            this.ColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLongitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLatitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLinkMan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLinkPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCreateDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mainStatusStrip.SuspendLayout();
            this.editPanel.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            this.explorerPanel.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            this.tablePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mainStatusStrip
            // 
            this.mainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.appStatusLabel,
            this.addressStatusLabel,
            this.lnglatStatusLabel});
            this.mainStatusStrip.Location = new System.Drawing.Point(0, 639);
            this.mainStatusStrip.Name = "mainStatusStrip";
            this.mainStatusStrip.Size = new System.Drawing.Size(1184, 22);
            this.mainStatusStrip.TabIndex = 4;
            this.mainStatusStrip.Text = "状态";
            // 
            // appStatusLabel
            // 
            this.appStatusLabel.Margin = new System.Windows.Forms.Padding(0, 3, 20, 2);
            this.appStatusLabel.Name = "appStatusLabel";
            this.appStatusLabel.Size = new System.Drawing.Size(32, 17);
            this.appStatusLabel.Text = "状态";
            // 
            // addressStatusLabel
            // 
            this.addressStatusLabel.Margin = new System.Windows.Forms.Padding(0, 3, 20, 2);
            this.addressStatusLabel.Name = "addressStatusLabel";
            this.addressStatusLabel.Size = new System.Drawing.Size(32, 17);
            this.addressStatusLabel.Text = "位置";
            // 
            // lnglatStatusLabel
            // 
            this.lnglatStatusLabel.Margin = new System.Windows.Forms.Padding(0, 3, 20, 2);
            this.lnglatStatusLabel.Name = "lnglatStatusLabel";
            this.lnglatStatusLabel.Size = new System.Drawing.Size(32, 17);
            this.lnglatStatusLabel.Text = "坐标";
            // 
            // nameTBox
            // 
            this.nameTBox.Location = new System.Drawing.Point(58, 64);
            this.nameTBox.Name = "nameTBox";
            this.nameTBox.Size = new System.Drawing.Size(174, 21);
            this.nameTBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 242);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "地址";
            // 
            // addressTBox
            // 
            this.addressTBox.Location = new System.Drawing.Point(58, 239);
            this.addressTBox.Name = "addressTBox";
            this.addressTBox.Size = new System.Drawing.Size(174, 97);
            this.addressTBox.TabIndex = 9;
            this.addressTBox.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "经度";
            // 
            // longitudeTBox
            // 
            this.longitudeTBox.Location = new System.Drawing.Point(58, 99);
            this.longitudeTBox.Name = "longitudeTBox";
            this.longitudeTBox.Size = new System.Drawing.Size(174, 21);
            this.longitudeTBox.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "纬度";
            // 
            // latitudeTBox
            // 
            this.latitudeTBox.Location = new System.Drawing.Point(58, 134);
            this.latitudeTBox.Name = "latitudeTBox";
            this.latitudeTBox.Size = new System.Drawing.Size(174, 21);
            this.latitudeTBox.TabIndex = 13;
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(58, 361);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(174, 23);
            this.saveBtn.TabIndex = 14;
            this.saveBtn.Text = "保存";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 15;
            this.label5.Text = "联系人";
            // 
            // linkManTBox
            // 
            this.linkManTBox.Location = new System.Drawing.Point(58, 169);
            this.linkManTBox.Name = "linkManTBox";
            this.linkManTBox.Size = new System.Drawing.Size(174, 21);
            this.linkManTBox.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 207);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 17;
            this.label6.Text = "电话";
            // 
            // linkPhoneTBox
            // 
            this.linkPhoneTBox.Location = new System.Drawing.Point(58, 204);
            this.linkPhoneTBox.Name = "linkPhoneTBox";
            this.linkPhoneTBox.Size = new System.Drawing.Size(174, 21);
            this.linkPhoneTBox.TabIndex = 18;
            // 
            // editPanel
            // 
            this.editPanel.Controls.Add(this.label7);
            this.editPanel.Controls.Add(this.idTBox);
            this.editPanel.Controls.Add(this.label1);
            this.editPanel.Controls.Add(this.linkPhoneTBox);
            this.editPanel.Controls.Add(this.nameTBox);
            this.editPanel.Controls.Add(this.label6);
            this.editPanel.Controls.Add(this.label2);
            this.editPanel.Controls.Add(this.linkManTBox);
            this.editPanel.Controls.Add(this.addressTBox);
            this.editPanel.Controls.Add(this.label5);
            this.editPanel.Controls.Add(this.label3);
            this.editPanel.Controls.Add(this.saveBtn);
            this.editPanel.Controls.Add(this.longitudeTBox);
            this.editPanel.Controls.Add(this.latitudeTBox);
            this.editPanel.Controls.Add(this.label4);
            this.editPanel.Location = new System.Drawing.Point(0, 50);
            this.editPanel.Name = "editPanel";
            this.editPanel.Size = new System.Drawing.Size(250, 580);
            this.editPanel.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 20;
            this.label7.Text = "编号";
            // 
            // idTBox
            // 
            this.idTBox.Enabled = false;
            this.idTBox.Location = new System.Drawing.Point(58, 29);
            this.idTBox.Name = "idTBox";
            this.idTBox.Size = new System.Drawing.Size(174, 21);
            this.idTBox.TabIndex = 19;
            // 
            // aMapBrowser
            // 
            this.aMapBrowser.IsWebBrowserContextMenuEnabled = false;
            this.aMapBrowser.Location = new System.Drawing.Point(255, 50);
            this.aMapBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.aMapBrowser.Name = "aMapBrowser";
            this.aMapBrowser.ScrollBarsEnabled = false;
            this.aMapBrowser.Size = new System.Drawing.Size(920, 580);
            this.aMapBrowser.TabIndex = 0;
            this.aMapBrowser.Url = new System.Uri("", System.UriKind.Relative);
            this.aMapBrowser.WebBrowserShortcutsEnabled = false;
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem,
            this.aboutMItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(1184, 25);
            this.mainMenuStrip.TabIndex = 5;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshMItem,
            this.exitMItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(44, 21);
            this.fileMenuItem.Text = "文件";
            // 
            // refreshMItem
            // 
            this.refreshMItem.Name = "refreshMItem";
            this.refreshMItem.Size = new System.Drawing.Size(100, 22);
            this.refreshMItem.Text = "刷新";
            this.refreshMItem.Click += new System.EventHandler(this.refreshMItem_Click);
            // 
            // exitMItem
            // 
            this.exitMItem.Name = "exitMItem";
            this.exitMItem.Size = new System.Drawing.Size(100, 22);
            this.exitMItem.Text = "退出";
            // 
            // aboutMItem
            // 
            this.aboutMItem.Name = "aboutMItem";
            this.aboutMItem.Size = new System.Drawing.Size(44, 21);
            this.aboutMItem.Text = "关于";
            this.aboutMItem.Click += new System.EventHandler(this.aboutMItem_Click);
            // 
            // explorerPanel
            // 
            this.explorerPanel.Controls.Add(this.explorerLBox);
            this.explorerPanel.Location = new System.Drawing.Point(0, 50);
            this.explorerPanel.Name = "explorerPanel";
            this.explorerPanel.Size = new System.Drawing.Size(250, 580);
            this.explorerPanel.TabIndex = 20;
            // 
            // explorerLBox
            // 
            this.explorerLBox.FormattingEnabled = true;
            this.explorerLBox.Location = new System.Drawing.Point(12, 9);
            this.explorerLBox.MultiColumn = true;
            this.explorerLBox.Name = "explorerLBox";
            this.explorerLBox.Size = new System.Drawing.Size(220, 564);
            this.explorerLBox.TabIndex = 0;
            this.explorerLBox.Click += new System.EventHandler(this.explorerLBox_Click);
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mapToolBtn,
            this.tableToolBtn,
            this.toolStripSeparator1,
            this.addToolBtn,
            this.editToolBtn,
            this.deleteToolBtn});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 25);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(1184, 25);
            this.mainToolStrip.TabIndex = 21;
            this.mainToolStrip.Text = "mainToolStrip";
            // 
            // addToolBtn
            // 
            this.addToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.addToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("addToolBtn.Image")));
            this.addToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addToolBtn.Name = "addToolBtn";
            this.addToolBtn.Size = new System.Drawing.Size(36, 22);
            this.addToolBtn.Text = "添加";
            this.addToolBtn.Click += new System.EventHandler(this.addToolBtn_Click);
            // 
            // editToolBtn
            // 
            this.editToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.editToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("editToolBtn.Image")));
            this.editToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editToolBtn.Name = "editToolBtn";
            this.editToolBtn.Size = new System.Drawing.Size(36, 22);
            this.editToolBtn.Text = "修改";
            this.editToolBtn.Click += new System.EventHandler(this.editToolBtn_Click);
            // 
            // deleteToolBtn
            // 
            this.deleteToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.deleteToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("deleteToolBtn.Image")));
            this.deleteToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteToolBtn.Name = "deleteToolBtn";
            this.deleteToolBtn.Size = new System.Drawing.Size(36, 22);
            this.deleteToolBtn.Text = "删除";
            this.deleteToolBtn.Click += new System.EventHandler(this.deleteToolBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // mapToolBtn
            // 
            this.mapToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.mapToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("mapToolBtn.Image")));
            this.mapToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mapToolBtn.Name = "mapToolBtn";
            this.mapToolBtn.Size = new System.Drawing.Size(60, 22);
            this.mapToolBtn.Text = "地图模式";
            this.mapToolBtn.Click += new System.EventHandler(this.mapToolBtn_Click);
            // 
            // tableToolBtn
            // 
            this.tableToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tableToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("tableToolBtn.Image")));
            this.tableToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tableToolBtn.Name = "tableToolBtn";
            this.tableToolBtn.Size = new System.Drawing.Size(60, 22);
            this.tableToolBtn.Text = "表格模式";
            this.tableToolBtn.Click += new System.EventHandler(this.tableToolBtn_Click);
            // 
            // tablePanel
            // 
            this.tablePanel.Controls.Add(this.explorerTable);
            this.tablePanel.Location = new System.Drawing.Point(0, 50);
            this.tablePanel.Name = "tablePanel";
            this.tablePanel.Size = new System.Drawing.Size(1184, 580);
            this.tablePanel.TabIndex = 22;
            // 
            // explorerTable
            // 
            this.explorerTable.AllowUserToAddRows = false;
            this.explorerTable.AllowUserToDeleteRows = false;
            this.explorerTable.AutoGenerateColumns = false;
            this.explorerTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.explorerTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnId,
            this.ColumnName,
            this.ColumnLongitude,
            this.ColumnLatitude,
            this.ColumnAddress,
            this.ColumnLinkMan,
            this.ColumnLinkPhone,
            this.ColumnCreateDate});
            this.explorerTable.DataSource = this.pointSource;
            this.explorerTable.Location = new System.Drawing.Point(12, 9);
            this.explorerTable.Name = "explorerTable";
            this.explorerTable.ReadOnly = true;
            this.explorerTable.RowTemplate.Height = 23;
            this.explorerTable.Size = new System.Drawing.Size(1160, 564);
            this.explorerTable.TabIndex = 0;
            // 
            // pointSource
            // 
            this.pointSource.DataSource = typeof(LocalViewer.Model.PointModel);
            // 
            // ColumnId
            // 
            this.ColumnId.DataPropertyName = "Id";
            this.ColumnId.HeaderText = "编号";
            this.ColumnId.Name = "ColumnId";
            this.ColumnId.ReadOnly = true;
            this.ColumnId.Width = 60;
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "Name";
            this.ColumnName.HeaderText = "名称";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.ReadOnly = true;
            // 
            // ColumnLongitude
            // 
            this.ColumnLongitude.DataPropertyName = "Longitude";
            this.ColumnLongitude.HeaderText = "经度";
            this.ColumnLongitude.Name = "ColumnLongitude";
            this.ColumnLongitude.ReadOnly = true;
            // 
            // ColumnLatitude
            // 
            this.ColumnLatitude.DataPropertyName = "Latitude";
            this.ColumnLatitude.HeaderText = "纬度";
            this.ColumnLatitude.Name = "ColumnLatitude";
            this.ColumnLatitude.ReadOnly = true;
            // 
            // ColumnAddress
            // 
            this.ColumnAddress.DataPropertyName = "Address";
            this.ColumnAddress.HeaderText = "地址";
            this.ColumnAddress.Name = "ColumnAddress";
            this.ColumnAddress.ReadOnly = true;
            this.ColumnAddress.Width = 400;
            // 
            // ColumnLinkMan
            // 
            this.ColumnLinkMan.DataPropertyName = "LinkMan";
            this.ColumnLinkMan.HeaderText = "联系人";
            this.ColumnLinkMan.Name = "ColumnLinkMan";
            this.ColumnLinkMan.ReadOnly = true;
            // 
            // ColumnLinkPhone
            // 
            this.ColumnLinkPhone.DataPropertyName = "LinkPhone";
            this.ColumnLinkPhone.HeaderText = "联系电话";
            this.ColumnLinkPhone.Name = "ColumnLinkPhone";
            this.ColumnLinkPhone.ReadOnly = true;
            this.ColumnLinkPhone.Width = 120;
            // 
            // ColumnCreateDate
            // 
            this.ColumnCreateDate.DataPropertyName = "CreateDate";
            this.ColumnCreateDate.HeaderText = "创建时间";
            this.ColumnCreateDate.Name = "ColumnCreateDate";
            this.ColumnCreateDate.ReadOnly = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.tablePanel);
            this.Controls.Add(this.mainToolStrip);
            this.Controls.Add(this.mainStatusStrip);
            this.Controls.Add(this.mainMenuStrip);
            this.Controls.Add(this.aMapBrowser);
            this.Controls.Add(this.explorerPanel);
            this.Controls.Add(this.editPanel);
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "MainForm";
            this.Text = "本地通";
            this.mainStatusStrip.ResumeLayout(false);
            this.mainStatusStrip.PerformLayout();
            this.editPanel.ResumeLayout(false);
            this.editPanel.PerformLayout();
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.explorerPanel.ResumeLayout(false);
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.tablePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip mainStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel appStatusLabel;
        public System.Windows.Forms.WebBrowser aMapBrowser;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitMItem;
        private System.Windows.Forms.ToolStripStatusLabel addressStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel lnglatStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem refreshMItem;
        private System.Windows.Forms.Panel explorerPanel;
        private System.Windows.Forms.CheckedListBox explorerLBox;
        private System.Windows.Forms.ToolStripMenuItem aboutMItem;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton deleteToolBtn;
        private System.Windows.Forms.ToolStripButton addToolBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton mapToolBtn;
        private System.Windows.Forms.ToolStripButton tableToolBtn;
        private System.Windows.Forms.TextBox nameTBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox addressTBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox longitudeTBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox latitudeTBox;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox linkManTBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox linkPhoneTBox;
        private System.Windows.Forms.Panel editPanel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox idTBox;
        private System.Windows.Forms.ToolStripButton editToolBtn;
        private System.Windows.Forms.Panel tablePanel;
        private System.Windows.Forms.DataGridView explorerTable;
        private System.Windows.Forms.BindingSource pointSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLongitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLatitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLinkMan;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLinkPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCreateDate;
    }
}

