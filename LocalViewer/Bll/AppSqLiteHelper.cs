﻿using LocalViewer.Comm;
using LocalViewer.Model;
using LocanFormsApp.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;

namespace LocalViewer.Bll
{
    public class AppSqlLiteHelper
    {

        public static void init()
        {
            SQLiteConnection conn = new SQLiteConnection(AppConfig.SqlLiteSource);
            conn.Open();
            string sql_create = @"create table if not exists lv_map(
                [id] integer primary key AUTOINCREMENT,
                [Name] varchar(50),
                [Address] varchar(100),
                [Longitude] varhcar(20),
                [Latitude] varchar(20),
                [LinkMan] varchar(20),
                [LinkPhone] varchar(20),
                [CreateDate] varchar(20)
            )";
            SQLiteHelper.ExecuteNonQuery(AppConfig.SqlLiteSource, sql_create, CommandType.Text);
            conn.Close();
        }

        public static void executeUpdate(String sql, SQLiteParameter ps)
        {

        }

        /// <summary>
        /// 添加标注信息
        /// </summary>
        /// <param name="name"></param>
        /// <param name="address"></param>
        /// <param name="longitude">经度</param>
        /// <param name="latitude">维度</param>
        public static bool add(PointModel model)
        {
            string sql = @"insert into lv_map(Name, Address, Longitude, Latitude, LinkMan, LinkPhone, CreateDate) 
                            values(@Name, @Address, @Longitude, @Latitude, @LinkMan, @LinkPhone, @CreateDate)";
            SQLiteParameter[] ps = new SQLiteParameter[] { 
                new SQLiteParameter("@Name", model.Name),
                new SQLiteParameter("@Address", model.Address),
                new SQLiteParameter("@Longitude", model.Longitude),
                new SQLiteParameter("@Latitude", model.Latitude),
                new SQLiteParameter("@LinkMan", model.LinkMan),
                new SQLiteParameter("@LinkPhone", model.LinkPhone),
                new SQLiteParameter("@CreateDate", model.CreateDate.ToString("yyyy-MM-dd HH:mm:ss")),
            };
            return SQLiteHelper.ExecuteNonQuery(AppConfig.SqlLiteSource, sql, CommandType.Text, ps) == 1;
        }

        /// <summary>
        /// 修改标注信息
        /// </summary>
        /// <param name="name"></param>
        /// <param name="address"></param>
        /// <param name="longitude">经度</param>
        /// <param name="latitude">维度</param>
        public static bool update(PointModel model)
        {
            string sql = @"update lv_map set 
                                    Name = @Name, 
                                    Address = @Address, 
                                    Longitude = @Longitude, 
                                    Latitude = @Latitude, 
                                    LinkMan = @LinkMan, 
                                    LinkPhone = @LinkPhone
                                    where Id = @Id ";
            SQLiteParameter[] ps = new SQLiteParameter[] { 
                new SQLiteParameter("@Id", model.Id),
                new SQLiteParameter("@Name", model.Name),
                new SQLiteParameter("@Address", model.Address),
                new SQLiteParameter("@Longitude", model.Longitude),
                new SQLiteParameter("@Latitude", model.Latitude),
                new SQLiteParameter("@LinkMan", model.LinkMan),
                new SQLiteParameter("@LinkPhone", model.LinkPhone),
            };
            return SQLiteHelper.ExecuteNonQuery(AppConfig.SqlLiteSource, sql, CommandType.Text, ps) == 1;
        }

        public static bool delete(PointModel model)
        {
            return delete(model.Id);
        }

        public static bool delete(long Id)
        {
            string sql = @"delete from lv_map where Id = @Id";
            SQLiteParameter[] ps = new SQLiteParameter[] { 
                new SQLiteParameter("@Id", Id)
            };
            return SQLiteHelper.ExecuteNonQuery(AppConfig.SqlLiteSource, sql, CommandType.Text, ps) == 1;
        }

        public static List<PointModel> getAll()
        {
            string sql = @"select Id, Name, Address, Longitude, Latitude, LinkMan, LinkPhone, CreateDate from lv_map";
            DataSet set = SQLiteHelper.ExecuteDataSet(AppConfig.SqlLiteSource, sql, CommandType.Text);

            List<PointModel> res = new List<PointModel>();
            foreach (DataRow row in set.Tables[0].Rows)
            {
                res.Add(new PointModel() 
                {
 
                    Id = (Int64)row["Id"],
                    Name = row["Name"].ToString(),
                    Address = row["Address"].ToString(),
                    Longitude = row["Longitude"].ToString(),
                    Latitude = row["Latitude"].ToString(),
                    LinkMan = row["LinkMan"].ToString(),
                    LinkPhone = row["LinkPhone"].ToString(),
                    CreateDate = DateTime.Parse(row["CreateDate"].ToString()),
                });
            }
            return res;
        }

    }
}
