﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalViewer.Comm
{
    public class AppConfig
    {
        private static String _sqliteSource = String.Empty;

        public static String SqlLiteSource
        {
            get
            {
                return _sqliteSource;
            }
        }


        public static void init()
        {
            //
            //_sqliteSource = String.Format(@"DataSource={0}App_Data\lvsqlite.db", System.AppDomain.CurrentDomain.BaseDirectory);
            _sqliteSource = String.Format(@"DataSource=E:\SQLiteDataBase\lvsqlite.db");
        }

    }
}
