﻿namespace LocalViewer.Form
{
    partial class EditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.appStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.addressStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lnglatStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.aMapBrowser = new System.Windows.Forms.WebBrowser();
            this.nameTBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addressTBox = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.longitudeTBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.latitudeTBox = new System.Windows.Forms.TextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.linkManTBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.linkPhoneTBox = new System.Windows.Forms.TextBox();
            this.editPanel = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.idTBox = new System.Windows.Forms.TextBox();
            this.pointSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainStatusStrip.SuspendLayout();
            this.editPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pointSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mainStatusStrip
            // 
            this.mainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.appStatusLabel,
            this.addressStatusLabel,
            this.lnglatStatusLabel});
            this.mainStatusStrip.Location = new System.Drawing.Point(0, 605);
            this.mainStatusStrip.Name = "mainStatusStrip";
            this.mainStatusStrip.Size = new System.Drawing.Size(1184, 22);
            this.mainStatusStrip.TabIndex = 4;
            this.mainStatusStrip.Text = "状态";
            // 
            // appStatusLabel
            // 
            this.appStatusLabel.Margin = new System.Windows.Forms.Padding(0, 3, 20, 2);
            this.appStatusLabel.Name = "appStatusLabel";
            this.appStatusLabel.Size = new System.Drawing.Size(32, 17);
            this.appStatusLabel.Text = "状态";
            // 
            // addressStatusLabel
            // 
            this.addressStatusLabel.Margin = new System.Windows.Forms.Padding(0, 3, 20, 2);
            this.addressStatusLabel.Name = "addressStatusLabel";
            this.addressStatusLabel.Size = new System.Drawing.Size(32, 17);
            this.addressStatusLabel.Text = "位置";
            // 
            // lnglatStatusLabel
            // 
            this.lnglatStatusLabel.Margin = new System.Windows.Forms.Padding(0, 3, 20, 2);
            this.lnglatStatusLabel.Name = "lnglatStatusLabel";
            this.lnglatStatusLabel.Size = new System.Drawing.Size(32, 17);
            this.lnglatStatusLabel.Text = "坐标";
            // 
            // aMapBrowser
            // 
            this.aMapBrowser.IsWebBrowserContextMenuEnabled = false;
            this.aMapBrowser.Location = new System.Drawing.Point(252, 12);
            this.aMapBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.aMapBrowser.Name = "aMapBrowser";
            this.aMapBrowser.ScrollBarsEnabled = false;
            this.aMapBrowser.Size = new System.Drawing.Size(920, 580);
            this.aMapBrowser.TabIndex = 0;
            this.aMapBrowser.Url = new System.Uri("", System.UriKind.Relative);
            this.aMapBrowser.WebBrowserShortcutsEnabled = false;
            // 
            // nameTBox
            // 
            this.nameTBox.Location = new System.Drawing.Point(58, 64);
            this.nameTBox.Name = "nameTBox";
            this.nameTBox.Size = new System.Drawing.Size(174, 21);
            this.nameTBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 242);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "地址";
            // 
            // addressTBox
            // 
            this.addressTBox.Location = new System.Drawing.Point(58, 239);
            this.addressTBox.Name = "addressTBox";
            this.addressTBox.Size = new System.Drawing.Size(174, 97);
            this.addressTBox.TabIndex = 9;
            this.addressTBox.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "经度";
            // 
            // longitudeTBox
            // 
            this.longitudeTBox.Location = new System.Drawing.Point(58, 99);
            this.longitudeTBox.Name = "longitudeTBox";
            this.longitudeTBox.Size = new System.Drawing.Size(174, 21);
            this.longitudeTBox.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "纬度";
            // 
            // latitudeTBox
            // 
            this.latitudeTBox.Location = new System.Drawing.Point(58, 134);
            this.latitudeTBox.Name = "latitudeTBox";
            this.latitudeTBox.Size = new System.Drawing.Size(174, 21);
            this.latitudeTBox.TabIndex = 13;
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(58, 361);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(174, 23);
            this.saveBtn.TabIndex = 14;
            this.saveBtn.Text = "保存";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 15;
            this.label5.Text = "联系人";
            // 
            // linkManTBox
            // 
            this.linkManTBox.Location = new System.Drawing.Point(58, 169);
            this.linkManTBox.Name = "linkManTBox";
            this.linkManTBox.Size = new System.Drawing.Size(174, 21);
            this.linkManTBox.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 207);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 17;
            this.label6.Text = "电话";
            // 
            // linkPhoneTBox
            // 
            this.linkPhoneTBox.Location = new System.Drawing.Point(58, 204);
            this.linkPhoneTBox.Name = "linkPhoneTBox";
            this.linkPhoneTBox.Size = new System.Drawing.Size(174, 21);
            this.linkPhoneTBox.TabIndex = 18;
            // 
            // editPanel
            // 
            this.editPanel.Controls.Add(this.label7);
            this.editPanel.Controls.Add(this.idTBox);
            this.editPanel.Controls.Add(this.label1);
            this.editPanel.Controls.Add(this.linkPhoneTBox);
            this.editPanel.Controls.Add(this.nameTBox);
            this.editPanel.Controls.Add(this.label6);
            this.editPanel.Controls.Add(this.label2);
            this.editPanel.Controls.Add(this.linkManTBox);
            this.editPanel.Controls.Add(this.addressTBox);
            this.editPanel.Controls.Add(this.label5);
            this.editPanel.Controls.Add(this.label3);
            this.editPanel.Controls.Add(this.saveBtn);
            this.editPanel.Controls.Add(this.longitudeTBox);
            this.editPanel.Controls.Add(this.latitudeTBox);
            this.editPanel.Controls.Add(this.label4);
            this.editPanel.Location = new System.Drawing.Point(0, 12);
            this.editPanel.Name = "editPanel";
            this.editPanel.Size = new System.Drawing.Size(250, 580);
            this.editPanel.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 20;
            this.label7.Text = "编号";
            // 
            // idTBox
            // 
            this.idTBox.Enabled = false;
            this.idTBox.Location = new System.Drawing.Point(58, 29);
            this.idTBox.Name = "idTBox";
            this.idTBox.Size = new System.Drawing.Size(174, 21);
            this.idTBox.TabIndex = 19;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 627);
            this.Controls.Add(this.mainStatusStrip);
            this.Controls.Add(this.aMapBrowser);
            this.Controls.Add(this.editPanel);
            this.Name = "EditForm";
            this.Text = "本地通";
            this.mainStatusStrip.ResumeLayout(false);
            this.mainStatusStrip.PerformLayout();
            this.editPanel.ResumeLayout(false);
            this.editPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pointSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip mainStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel appStatusLabel;
        public System.Windows.Forms.WebBrowser aMapBrowser;
        private System.Windows.Forms.ToolStripStatusLabel addressStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel lnglatStatusLabel;
        private System.Windows.Forms.TextBox nameTBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox addressTBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox longitudeTBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox latitudeTBox;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox linkManTBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox linkPhoneTBox;
        private System.Windows.Forms.Panel editPanel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox idTBox;
        private System.Windows.Forms.BindingSource pointSource;
    }
}

