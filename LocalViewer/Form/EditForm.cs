﻿using LocalViewer.Bll;
using LocalViewer.Comm;
using LocalViewer.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LocalViewer.Form
{
    [ComVisible(true)]//必须设置com+可见
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]//
    public partial class EditForm : System.Windows.Forms.Form
    {

        public EditForm()
        {
            InitializeComponent();
            InitAMap();
            AppConfig.init();
            AppSqlLiteHelper.init();
        }

        private void InitAMap()
        {

            this.aMapBrowser.ObjectForScripting = this;//必须指定网页脚本可访问的对象，这里指定js脚本可以访问当前对象
            this.aMapBrowser.Navigate(Application.StartupPath + @"\App_Data\web\index.html");
        }


        #region call js

        public void callJs_loadList(List<PointModel> points)
        {
            this.aMapBrowser.Document.InvokeScript("loadPoints", new object[] { JsonConvert.SerializeObject(points) });
        }

        public void callJs_selectPoint(PointModel point)
        {
            this.aMapBrowser.Document.InvokeScript("selectPoint", new object[] { JsonConvert.SerializeObject(point) });
        }


        #endregion

        #region js call

        public void js_addMarker(String address, String longitude, String latitude)
        {

        }

        public void js_setStatus(String msg)
        {
            this.appStatusLabel.Text = msg;
        }

        public void js_setAddress(String msg)
        {
            this.addressStatusLabel.Text = msg;
            this.addressTBox.Text = msg;
        }

        public void js_setLnglat(double longitude, double latitude)
        {
            this.lnglatStatusLabel.Text = longitude + "," + latitude;
            this.longitudeTBox.Text = longitude.ToString();
            this.latitudeTBox.Text = latitude.ToString();
        }
        #endregion

        private void saveBtn_Click(object sender, EventArgs e)
        {

            try
            {
                if (AppSqlLiteHelper.add(new PointModel()
                    {
                        Name = this.nameTBox.Text,
                        Address = this.addressTBox.Text,
                        Latitude = this.latitudeTBox.Text,
                        Longitude = this.longitudeTBox.Text,
                        LinkMan = this.linkManTBox.Text,
                        LinkPhone = this.linkPhoneTBox.Text,
                        CreateDate = DateTime.Now
                    }))
                {
                    MessageBox.Show("保存成功");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("保存失败：" + ex.Message);
            }
        }

    }
}
