﻿using LocalViewer.Bll;
using LocalViewer.Comm;
using LocalViewer.Form;
using LocalViewer.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LocalViewer
{
    [ComVisible(true)]//必须设置com+可见
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]//
    public partial class MainForm : System.Windows.Forms.Form
    {
        /// <summary>
        /// 当前显示模式
        /// </summary>
        private int _CurrentView = 3;

        /// <summary>
        /// 添加模式
        /// </summary>
        private const int VIEW_ADD = 1;
        /// <summary>
        /// 编辑模式
        /// </summary>
        private const int VIEW_EDIT = 2;
        /// <summary>
        /// 地图模式
        /// </summary>
        private const int VIEW_MAP = 3;
        /// <summary>
        /// 表格模式
        /// </summary>
        private const int VIEW_TABLE = 4;

        public MainForm()
        {
            InitializeComponent();
            InitAMap();
            AppConfig.init();
            AppSqlLiteHelper.init();

            if (this._CurrentView == VIEW_MAP)
            {
                this.tablePanel.Visible = false;
                this.editPanel.Visible = false;
            }
        }

        private void InitAMap()
        {

            this.aMapBrowser.ObjectForScripting = this;//必须指定网页脚本可访问的对象，这里指定js脚本可以访问当前对象
            this.aMapBrowser.Navigate(Application.StartupPath + @"\App_Data\web\index.html");
        }


        #region call js

        public void callJs_loadList(List<PointModel> points)
        {
            this.aMapBrowser.Document.InvokeScript("loadPoints", new object[] { JsonConvert.SerializeObject(points) });
        }

        public void callJs_selectPoint(PointModel point)
        {
            this.aMapBrowser.Document.InvokeScript("selectPoint", new object[] { JsonConvert.SerializeObject(point) });
        }


        #endregion

        #region js call


        public void js_setStatus(String msg)
        {
            this.appStatusLabel.Text = msg;
        }

        public void js_setAddress(String msg)
        {
            this.addressStatusLabel.Text = msg;
            this.addressTBox.Text = msg;
        }

        public void js_setLnglat(double longitude, double latitude)
        {
            this.lnglatStatusLabel.Text = longitude + "," + latitude;
            this.longitudeTBox.Text = longitude + "";
            this.latitudeTBox.Text = latitude + "";
        }

        public void js_mapLoadOver()
        {
            setMapMode();
        }
        #endregion

        #region 菜单栏按钮事件
        private void refreshMItem_Click(object sender, EventArgs e)
        {
            this.aMapBrowser.Refresh();
        }

        private void aboutMItem_Click(object sender, EventArgs e)
        {
            AboutLocalViewer form = new AboutLocalViewer();
            form.ShowDialog();
        }

        #endregion

        private void explorerLBox_Click(object sender, EventArgs e)
        {
            callJs_selectPoint((PointModel)this.explorerLBox.SelectedItem);
        }
        #region 工具栏点击事件

        private void addToolBtn_Click(object sender, EventArgs e)
        {
            this.editToolBtn.Enabled = false;
            this.deleteToolBtn.Enabled = false;

            this.clearEditPanel();

            this.editPanel.Visible = true;
            this.explorerPanel.Visible = false;
            this.tablePanel.Visible = false;
            this._CurrentView = VIEW_ADD;
        }
        private void editToolBtn_Click(object sender, EventArgs e)
        {
            this.editToolBtn.Enabled = false;
            this.deleteToolBtn.Enabled = false;
            PointModel item = null;

            if (this._CurrentView == VIEW_MAP)
                item = (PointModel)this.explorerLBox.SelectedItem;
            else if (this._CurrentView == VIEW_TABLE)
                item = (PointModel)this.explorerTable.CurrentRow.DataBoundItem;

            callJs_selectPoint(item);

            this.idTBox.Text = item.Id + "";
            this.nameTBox.Text = item.Name;
            this.longitudeTBox.Text = item.Longitude;
            this.latitudeTBox.Text = item.Latitude;
            this.linkManTBox.Text = item.LinkMan;
            this.linkPhoneTBox.Text = item.LinkPhone;
            this.addressTBox.Text = item.Address;

            this.editPanel.Visible = true;
            this.explorerPanel.Visible = false;
            this.tablePanel.Visible = false;
            this._CurrentView = VIEW_EDIT;
        }
        private void deleteToolBtn_Click(object sender, EventArgs e)
        {

            if (this._CurrentView == VIEW_MAP)
            {
                if (this.explorerLBox.CheckedItems.Count == 0)
                {
                    MessageBox.Show("请选择你希望删除的坐标信息");
                    return;
                }
                foreach (var item in this.explorerLBox.CheckedItems)
                {
                    AppSqlLiteHelper.delete((PointModel)item);
                }
                MessageBox.Show("删除成功");
                setMapMode();
            }
            else
            {
                String name = (String)this.explorerTable.CurrentRow.Cells[1].Value;
                DialogResult res = MessageBox.Show("您确定要删除 " + name + " ?", "警告", MessageBoxButtons.OKCancel);
                if (res == System.Windows.Forms.DialogResult.OK)
                {
                    if (AppSqlLiteHelper.delete((PointModel)this.explorerTable.CurrentRow.DataBoundItem))
                    {
                        MessageBox.Show("删除成功");
                        List<PointModel> list = AppSqlLiteHelper.getAll();
                        this.explorerTable.DataSource = list;
                    }
                }
            }
        }

        private void tableToolBtn_Click(object sender, EventArgs e)
        {
            this._CurrentView = VIEW_TABLE;
            this.editToolBtn.Enabled = true;
            this.deleteToolBtn.Enabled = true;
            List<PointModel> list = AppSqlLiteHelper.getAll();
            this.explorerTable.DataSource = list;

            this.editPanel.Visible = false;
            this.explorerPanel.Visible = false;
            this.tablePanel.Visible = true;
        }

        private void mapToolBtn_Click(object sender, EventArgs e)
        {
            setMapMode();
        }

        #endregion


        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (this.idTBox.Text.Length > 0)
            {
                try
                {
                    if (AppSqlLiteHelper.update(new PointModel()
                                {
                                    Id = long.Parse(this.idTBox.Text),
                                    Name = this.nameTBox.Text,
                                    Address = this.addressTBox.Text,
                                    Latitude = this.latitudeTBox.Text,
                                    Longitude = this.longitudeTBox.Text,
                                    LinkMan = this.linkManTBox.Text,
                                    LinkPhone = this.linkPhoneTBox.Text
                                }))
                    {
                        MessageBox.Show("修改成功");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("修改失败：" + ex.Message);
                }
            }
            else
            {
                try
                {
                    if (AppSqlLiteHelper.add(new PointModel()
                                {
                                    Name = this.nameTBox.Text,
                                    Address = this.addressTBox.Text,
                                    Latitude = this.latitudeTBox.Text,
                                    Longitude = this.longitudeTBox.Text,
                                    LinkMan = this.linkManTBox.Text,
                                    LinkPhone = this.linkPhoneTBox.Text,
                                    CreateDate = DateTime.Now
                                }))
                    {
                        MessageBox.Show("添加成功");
                        clearEditPanel();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("添加失败：" + ex.Message);
                }
            }
        }



        #region Common Method
        /// <summary>
        /// 清空编辑框
        /// </summary>
        private void clearEditPanel()
        {
            this.idTBox.Text = "";
            this.nameTBox.Text = "";
            this.longitudeTBox.Text = "";
            this.latitudeTBox.Text = "";
            this.linkManTBox.Text = "";
            this.linkPhoneTBox.Text = "";
            this.addressTBox.Text = "";
        }

        /// <summary>
        /// 设置地图显示模式
        /// </summary>
        public void setMapMode()
        {
            this._CurrentView = VIEW_MAP;
            this.editToolBtn.Enabled = true;
            this.deleteToolBtn.Enabled = true;

            try
            {
                List<PointModel> list = AppSqlLiteHelper.getAll();
                this.explorerLBox.DataSource = list;
                this.explorerLBox.DisplayMember = "Name";
                callJs_loadList(list);
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取信息失败：" + ex.Message);
            }

            this.editPanel.Visible = false;
            this.explorerPanel.Visible = true;
            this.tablePanel.Visible = false;
        }

        #endregion





    }
}
