﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalViewer.Model
{
    public class PointModel
    {
        public Int64 Id { get; set; }

        public String Name { get; set; }

        public String LinkMan { get; set; }

        public String LinkPhone { get; set; }

        public String Address { get; set; }

        /// <summary>
        /// 维度
        /// </summary>
        public String Latitude { get; set; }

        /// <summary>
        /// 经度
        /// </summary>
        public String Longitude { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; }
    }
}
